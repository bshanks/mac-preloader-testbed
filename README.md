**Wine macOS preloader testbed**

A standalone testbed for testing and fixing issues found with Wine's preloader on macOS.

`preloader_mac.c` is taken from Wine's `loader/preloader_mac.c`, but with modifications to build standalone and with the memory-reservation functionality removed.

Build with `./build.sh`, and run with `./preloader-x86_64 cocoatest-x86_64-unixthread` (matches how Wine's loader is built) or `./preloader-x86_64 cocoatest-x86_64-main`.

Or pass `-i386` to `./build.sh` to build for i386. Output files are named with `i386` instead of `x86_64`.

Bugs you can explore with this tool:
- [WineHQ bug 52354](https://bugs.winehq.org/show_bug.cgi?id=52354): AppKit layer-backed views do not update correctly on 10.13 and earlier. This is fixed/worked-around by building the launched binary (`cocoatest` in this testbed) as an `LC_UNIXTHREAD` binary by specifying `-mmacosx-version-min=10.7`.
- [WineHQ bug 54009](https://bugs.winehq.org/show_bug.cgi?id=54009): An `LC_UNIXTHREAD` binary launched by the preloader has a NULL `environ` on macOS 12 and 13. This is fixed/worked-around by setting `environ` manually in the preloader before jumping to the binary.

Brendan Shanks
bshanks@codeweavers.com

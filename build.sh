#!/bin/sh

ARCH="x86_64"
if [ "$1" = "-i386" ]; then
	ARCH="i386"
fi;

clang -arch $ARCH -o cocoatest-$ARCH-unixthread cocoatest.m -framework AppKit -fno-objc-arc -mmacosx-version-min=10.7
clang -arch $ARCH -o cocoatest-$ARCH-main cocoatest.m -framework AppKit -fno-objc-arc

gcc -arch $ARCH -c -o preloader_mac-$ARCH.o preloader_mac.c -D__WINESRC__   -Wall -pipe -fno-stack-protector -fno-strict-aliasing -Wdeclaration-after-statement -Wempty-body   -Wignored-qualifiers -Winit-self -Wno-pragma-pack -Wstrict-prototypes -Wtype-limits -Wvla   -Wwrite-strings -Wpointer-arith -gdwarf-4 -g -O2 -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=0
gcc -arch $ARCH -o preloader-$ARCH preloader_mac-$ARCH.o -Wl,-no_pie -mmacosx-version-min=10.7   -Wl,-no_new_main -nostartfiles -nodefaultlibs -e _start -ldylib1.o   -Wl,-image_base,0x7d400000,-segalign,0x1000,-pagezero_size,0x1000,-sectcreate,__TEXT,__info_plist,wine_info.plist

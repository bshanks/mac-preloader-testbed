
#import <AppKit/AppKit.h>
#include <dlfcn.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <crt_externs.h>
#include <mach-o/ldsyms.h>

extern int NXArgc;
extern char **NXArgv;
extern char **environ;
extern const char *__progname;

void *wine_main_preload_info = NULL;

@interface ContentLayerView : NSView {
NSTimer *timer;
int color;
}

@end

@interface ContentLayerView ()

@property (readwrite, nonatomic) NSTimer *timer;
@property (readwrite, nonatomic) int color;

@end

@implementation ContentLayerView

@synthesize color, timer;

- (instancetype)initWithFrame:(NSRect)frameRect {
    NSLog(@"initWithFrame");
    self = [super initWithFrame:frameRect];
    if (self)
    {
        self.color = 0;
        self.timer = [NSTimer timerWithTimeInterval:3.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
            self.color++;
            [self setNeedsDisplay:YES];
            NSLog(@"timer color now %d, calling setNeedsDisplay:", self.color);
        }];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    }
    return self;
}

- (BOOL) wantsUpdateLayer
{
    return YES;
}

- (void) updateLayer
{
    CALayer* layer = [self layer];
    NSLog(@"updateLayer layer %@", layer);
    size_t width = layer.bounds.size.width;
    size_t height = layer.bounds.size.height;

    NSMutableData *data = [NSMutableData dataWithLength:(width * height * 4)];
    uint32_t *datap = [data mutableBytes];
    for (size_t i = 0; i < (height*width); i++)
    {
        switch (self.color % 4)
        {
            case 0:
                datap[i] |= 0xff;
                break;
            case 1:
                datap[i] |= 0xff0000;
                break;
            case 2:
                datap[i] |= 0xff00;
                break;
            case 3:
                break;
        }
    }

    CGColorSpaceRef colorspace = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    CGImageRef img = CGImageCreate(width, height, 8, 32, (width * 8 * 4) / 8, colorspace, kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Little, provider, NULL, NO, kCGRenderingIntentDefault);

    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorspace);

    layer.contents = (id)img;
    CFRelease(img);
}

- (void) viewWillDraw
{
    [super viewWillDraw];

    NSLog(@"viewWillDraw");
}

@end

@interface ApplicationController : NSObject <NSApplicationDelegate>

@end

@implementation ApplicationController

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSLog(@"applicationDidFinishLaunching");
    NSRect frame = NSMakeRect(0, 0, 400, 400);
    NSWindow *window = [[NSWindow alloc] initWithContentRect:frame styleMask:NSWindowStyleMaskTitled backing:NSBackingStoreBuffered defer:NO];
    [window makeKeyAndOrderFront:NSApp];

    ContentLayerView *view = [[ContentLayerView alloc] initWithFrame:NSMakeRect(0, 0, 400, 400)];
    [view setWantsLayer:YES];
    [window setContentView:view];
}

@end

int main(int argc, char **argv)
{
    fprintf(stderr, "argc %d NXArgc %d &NXArgc %p _NSGetArgc %d _NSGetArgc %p\n", argc, NXArgc, &NXArgc, *_NSGetArgc(), _NSGetArgc());
    if (argc != NXArgc)
        fprintf(stderr, "**** argc (%d) doesn't match NXArgc (%d)\n", argc, NXArgc);
    if (argc != *_NSGetArgc())
        fprintf(stderr, "**** argc (%d) doesn't match _NSGetArgc() (%d)\n", argc, *_NSGetArgc());

    fprintf(stderr, "argv %p NXArgv %p &NXArgv %p _NSGetArgv %p _NSGetArgv %p\n", argv, NXArgv, &NXArgv, *_NSGetArgv(), _NSGetArgv());
    if (argv != NXArgv)
        fprintf(stderr, "**** argv (%p) doesn't match NXArgv (%p)\n", argv, NXArgv);
    if (argv != *_NSGetArgv())
        fprintf(stderr, "**** argv (%p) doesn't match *_NSGetArgv() (%p)\n", argv, *_NSGetArgv());

    fprintf(stderr, "&environ %p environ %p *NSGetEnviron() %p\n", &environ, environ, *_NSGetEnviron());
    if (environ != *_NSGetEnviron())
        fprintf(stderr, "**** environ (%p) doesn't match _NSGetEnviron() (%p)\n", environ, *_NSGetEnviron());

    fprintf(stderr, "__progname %s &__progname %p _NSGetProgname %s &_NSGetProgname %p\n", __progname, &__progname, *_NSGetProgname(), _NSGetProgname());
    if (__progname != *_NSGetProgname())
        fprintf(stderr, "**** __progname (%p) doesn't match _NSGetProgname() (%p)\n", __progname, *_NSGetProgname());

    fprintf(stderr, "_mh_execute_header %p _NSGetMachExecuteHeader() %p\n", &_mh_execute_header, _NSGetMachExecuteHeader());
    /* This test fails on LC_MAIN, but succeeds on LC_UNIXTHREAD.
     * I don't know how to fix this one (maybe dyld interposing?), but it could be causing other bugs (like the AppKit/Core Animation issue).
     */
    if (&_mh_execute_header != _NSGetMachExecuteHeader())
        fprintf(stderr, "**** _mh_execute_header (%p) doesn't match _NSGetMachExecuteHeader() (%p)\n", &_mh_execute_header, _NSGetMachExecuteHeader());

    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    [NSApplication sharedApplication];

    ApplicationController *appController = [[ApplicationController alloc] init];
    [[NSApplication sharedApplication] setDelegate:appController];

    [pool release];

    [NSApp run];
    return 0;
}
